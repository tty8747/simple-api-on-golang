include env

.DEFAULT_GOAL := all

# the same that .DEFAULT_GOAL for older version of make (<= 3.80)
# .PHONY: default
# default: clean;

.PHONY: all
all: build run clean

.PHONY:
init:
	$(info -> Only run it once!)
	go mod init ${PJ_PATH}

.PHONY: build
build:
	$(info -> Build:)
	go mod tidy
	mkdir -p ./bin
	GOARCH=${GOARCH} GOOS=${GOOS} go build -o ./bin/${BINARY_NAME}-linux ${PATH_TO_SOURCE}

.PHONY: run
run:
	$(info -> Run:)
	./bin/${BINARY_NAME}-linux

.PHONY: clean
clean:
	$(info -> Clean:)
	go clean
	rm ./bin/${BINARY_NAME}-linux
	rmdir ./bin

.PHONY: messages
messages:
	$(info test info message)
	$(warning test warning message)
	$(error test error message)
