package main

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/tty8747/simple-api-on-golang/internal/routes"
)

func main() {
	app := fiber.New()

	// Attach the newly created route to the http.Server
	routes.Route(app)

	app.Listen(":3000")

}
