package controllers

import (
	"log"
	"net/http"
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/tty8747/simple-api-on-golang/internal/models"
	"gitlab.com/tty8747/simple-api-on-golang/internal/responses"
)

func SomeAnswer(c *fiber.Ctx) error {

	result := models.User{
		Id:       "0",
		Name:     "John",
		Location: "Unknown",
		Title:    "Some",
	}

	return c.Status(http.StatusOK).JSON(responses.Response{Status: http.StatusOK, Message: "success", Data: &fiber.Map{"data": result}})
}

func Hello(c *fiber.Ctx) error {
	m := make(map[string]string)
	hostname, err := os.Hostname()
	if err != nil {
		m["Error"] = error.Error(err)
	}
	m["os.Hostname()"] = hostname
	m["Hostname"] = c.Hostname()
	m["URL"] = c.BaseURL()
	m["IP"] = c.IP()
	m["Original URL"] = c.OriginalURL()

	log.Printf("Got request: %s", c.BaseURL())

	return c.Status(http.StatusOK).JSON(m)
}
