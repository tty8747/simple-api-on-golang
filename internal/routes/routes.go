package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/tty8747/simple-api-on-golang/internal/controllers"
)

func Route(app *fiber.App) {
	app.Get("/get", controllers.SomeAnswer)
	app.Get("/hello", controllers.Hello)
}
